
# Cryptomate - a Proof of Concept of the Integration of the Searchitect Framework into an Application

## Purpose of this software
Extending Cryptomator with a search feature
* [Searchitect Framework] (https://github.com/cryptomator/cryptomator) enables search over encrypted files.
* [Cryptomator] (https://github.com/cryptomator/cryptomator) offers multi-platform transparent client-side encryption of your files in the cloud.

![cryptomator goes cryptomate](cryptomate.png)

*Warning: Cryptomate is not secure and not matured software and made just for demonstration purposes.*

## Description
We had to adapt the Cryptomator desktop application (Version 1.4.0-SNAPSHOT) which synchronizes the files stored in encrypted vaults to the cloud, because the code base of the Android App is proprietary. But as these applications are interoperable we decided to give a proof of concept for an integration of a search over encrypted files in the cryptomator cloud vaults.
Therefore we added this search feature by modifying the graphical user interface based on java-fx framework and adding an Searchitect class which accomplishes the method calls to the client class of the Searchitect framework. In this example we trigger the index generation process after some new files have been added by closing the vault.

Searchitect vault password is derived from vault password.

## Known flaws
* Searchitect username, password and url is stored in plaintext in the Cryptomator settings (~/.Cryptomator/settings.json)
```
    {
      "directories": [
      {
        "id": "upJGo1xHRH7D",
        "path": "/yourpath/new",
        "mountName": "new",
        "unlockAfterStartup": false,
        "revealAfterMount": true,
        "usesIndividualMountPath": false,
        "isSearchable": true,
        "searchUrl": "https://localhost:8433/",
        "searchUser": "test",
        "searchPwd": "test",
        "reponame": "863d86e6-af66-4ded-8701-295a10230a82",
        "isInitialized": true
    }
    ],
    ...
    }
```
* Client state is still just stored in memory


## Testing and Development environment
* Ubuntu 18.04
* IntelliJ IDEA Community Edition

### Usage
Make sure the Searchitect Framework is deployed at a known url e.g. https://localhost:8433/

* Run with OpenJDK 10 and Javafx
```
    export PATH_TO_FX=/your/path/to/javafx-sdk-11/lib
    java --module-path $PATH_TO_FX --add-modules=javafx.controls,javafx.fxml -jar Cryptomator-1.4.0-SNAPSHOT.jar
```
