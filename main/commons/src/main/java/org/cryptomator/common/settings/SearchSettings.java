package org.cryptomator.common.settings;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

import javax.inject.Inject;
import java.util.function.Consumer;

public class SearchSettings {
    public static final String DEFAULT_SEARCHURL = "https://localhost:8433/";
    public static final String DEFAULT_SEARCHUSER = null;
    public static final String DEFAULT_SEARCHPWD = null;
    public static final String DEFAULT_REPONAME = null;
    public static final Boolean DEFAULT_ISINITIALIZED = false;

    private final StringProperty searchUrl = new SimpleStringProperty(DEFAULT_SEARCHURL);
    private final StringProperty searchUser = new SimpleStringProperty(DEFAULT_SEARCHUSER);
    private final StringProperty searchPwd = new SimpleStringProperty(DEFAULT_SEARCHPWD);
    private final StringProperty reponame = new SimpleStringProperty(DEFAULT_REPONAME);
    private final BooleanProperty isInitialized = new SimpleBooleanProperty(DEFAULT_ISINITIALIZED);


    private Consumer<Settings> saveCmd;


    /**
     * Package-private constructor; use {@link SettingsProvider}.
     */
    @Inject
    SearchSettings() {
        searchUrl.addListener(this::somethingChanged);
        searchUser.addListener(this::somethingChanged);
        searchPwd.addListener(this::somethingChanged);

    }

    private void somethingChanged(ObservableValue<?> observable, Object oldValue, Object newValue) {
        this.save();
    }

    void setSaveCmd(Consumer<Settings> saveCmd) {
        this.saveCmd = saveCmd;
    }

    void save() {
        if (saveCmd != null) {
          //  saveCmd.accept(this);
        }
    }

    public StringProperty searchUrl() {
        return searchUrl;
    }

    public StringProperty searchUser()  {
        return  searchUser;
    }

    public StringProperty searchPwd()  {
        return  searchPwd;
    }

    public StringProperty reponame() {
        return reponame;
    }

    public BooleanProperty isInitialized(){
        return isInitialized;
    }
}
