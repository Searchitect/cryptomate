package org.cryptomator.common.settings;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class SearchSettingsJsonAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(SearchSettingsJsonAdapter.class);

    public void write(JsonWriter out, SearchSettings value) throws IOException {
        out.beginObject();
        out.name("searchUrl").value(value.searchUrl().get());
        out.name("searchUser").value(value.searchUser().get());
        out.name("searchPwd").value(value.searchPwd().get());
        out.name("reponame").value(value.reponame().get());
        out.name("isInitialized").value(value.isInitialized().get());
        out.endObject();
    }



    public SearchSettings read(JsonReader in) throws IOException {

        String searchUrl = null;
        String searchUser = null;
        String searchPwd = null;
        String reponame = null;
        boolean isInitialized = SearchSettings.DEFAULT_ISINITIALIZED;

        in.beginObject();
        while (in.hasNext()) {
            String name = in.nextName();
            switch (name) {
                case "searchUrl":
                    searchUrl = in.nextString();
                    break;
                case "searchUser":
                    searchUser = in.nextString();
                    break;
                case "searchPwd":
                    searchPwd = in.nextString();
                    break;
                case "reponame":
                    reponame = in.nextString();
                    break;
                case "isInitialized":
                    isInitialized = in.nextBoolean();
                    break;
                default:
                    LOG.warn("Unsupported vault setting found in JSON: " + name);
                    in.skipValue();
            }
        }

        in.endObject();

        SearchSettings searchSettings = new SearchSettings();
        searchSettings.searchUrl().set(searchUrl);
        searchSettings.searchUser().set(searchUser);
        searchSettings.searchPwd().set(searchPwd);
        searchSettings.reponame().set(reponame);
        searchSettings.isInitialized().set(isInitialized);

        return searchSettings;

    }
}
