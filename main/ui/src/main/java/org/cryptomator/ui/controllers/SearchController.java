package org.cryptomator.ui.controllers;

import javafx.application.Application;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import org.cryptomator.ui.l10n.Localization;
import org.cryptomator.ui.model.Vault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

public class SearchController implements ViewController{

    private static final Logger LOG = LoggerFactory.getLogger(SearchController .class);
    private final Application app;
    private final Localization localization;
    private Vault vault;

    @Inject
    public SearchController(Application app, Localization localization) {
        this.app = app;
        this.localization = localization;
    }

    void setVault(Vault vault) {
        this.vault = Objects.requireNonNull(vault);
        // trigger "default" change to refresh key bindings:
        searchButton.setDefaultButton(false);
        searchButton.setDefaultButton(true);
        responseText.setText(null);
    }


    @FXML
    private GridPane root;

    @FXML
    private TextField searchKeywordField;

    @FXML
    private Button searchButton;

    @FXML
    private Text responseText;

    @Override
    public void focus() {
        searchKeywordField.requestFocus();
    }


    @Override
    public Parent getRoot() {
        return root;
    }

    @Override
    public void initialize() {
        BooleanBinding searchKeywordFieldIsEmpty = searchKeywordField.textProperty().isEmpty();
        searchButton.disableProperty().bind(searchKeywordFieldIsEmpty);

    }

    @FXML
    private void  didClickSearchButton(ActionEvent event) {

        try {
            List<String> result =vault.searchitect.search(searchKeywordField.getText());
            responseText.setText("search result is: "+ result);
        }
        catch(Exception e){

        }
        }
}
