package org.cryptomator.ui.model;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ArrayListMultimap;
import org.cryptomator.common.settings.VaultSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import searchitect.client.Client;
import searchitect.client.dynrh2levplugin.Clientdynrh2levrocksImpl;
import searchitect.common.client.ClientScheme;
import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.crypto.CryptoPrimitives;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.common.view.UserAuthRequest;
import searchitect.common.view.UserAuthResponse;

import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

public class Searchitect {

    private static final Logger LOG = LoggerFactory.getLogger(Searchitect.class);
    private final Client searchclient;
    private final ClientScheme clientimpl;
    private final VaultSettings vaultSettings;
    private String token = "";
    private String password;


    public Searchitect(VaultSettings vaultSettings, CharSequence passphrase) {
        this.vaultSettings = vaultSettings;
        try {
            this.password = Base64.getEncoder().encodeToString(CryptoPrimitives.keyGenSetM(passphrase.toString(),vaultSettings.path().toString().getBytes(),1000,128));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException e) {
            this.password = passphrase.toString();
           LOG.error("Set searchitect password to passphrase becuase of {} ",e);
        }
        clientimpl = new Clientdynrh2levrocksImpl();
        searchclient = new Client(vaultSettings.searchUrl().get(), clientimpl);
    }


    /**
     * Setup or Update of the SE client
     * @param vaultSettings
     */
    public void setupOrUpdate(VaultSettings vaultSettings) {
        LOG.trace("searchitect setup or update");
        // create inverted index
        String dir = System.getProperty("java.io.tmpdir").concat("/").concat(vaultSettings.mountName().get());
        Path dirPath = Paths.get(dir);
        if (Files.exists(dirPath)) {
            try {
                ArrayList<File> fileList = new ArrayList<File>();
                // empty previous multimap
                TextExtractPar.lp1 = ArrayListMultimap.create();
                TextExtractPar.lp2 = ArrayListMultimap.create();
                TextProc.listf(dirPath.toString(), fileList);
                int fileCount = fileList.size();
                TextProc.TextProc(false, dirPath.toString());

                //check authentication
                if (vaultSettings.isInitialized().get()) {
                    authenticate();
                } else {
                    createUser();
                    authenticate();
                }
                //setup or update
                if (vaultSettings.isInitialized().get()) {
                    searchclient.update(vaultSettings.reponame().get(), TextExtractPar.lp1, vaultSettings.searchUser().get());
                } else {
                    RepositoryInfo repoInfo = searchclient.setup(password, TextExtractPar.lp1, fileCount);
                    vaultSettings.reponame().set(repoInfo.getrepositoryName());
                    vaultSettings.isInitialized().set(true);
                }
                recursivelyDelete(dirPath);
            } catch
            (IOException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | InvalidKeySpecException
                            e) {
                LOG.error("searchitect: {}", e.getMessage());
            }
        } else {
            LOG.error("No temporary directory found.");
        }

    }

    /**
     * Search for a keyword
     *
     * @param keyword
     * @return List of document identifiers (String)
     */
    public List<String> search(String keyword) {

        SearchResult searchResult = searchclient.search(vaultSettings.reponame().get(), keyword);
        return searchResult.getResultList();
    }


    private void authenticate() {
        UserAuthResponse token = null;
        try {
            token = searchclient.authenticateUser(new UserAuthRequest(vaultSettings.searchUser().get(), vaultSettings.searchPwd().get()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        searchclient.setJwToken(token.getToken());
        int i = searchclient.checkAuthBackend().getStatusCodeValue();
        LOG.trace("searchitect authentication {}", i);
    }

    private void createUser() {
        try {
            searchclient.createUser(new UserAuthRequest(vaultSettings.searchUser().get(), vaultSettings.searchPwd().get()));
        } catch (Exception e) {
            LOG.trace("User exists");
        }
    }

    private void recursivelyDelete(Path dirPath) {
        try {
            Files.walk(dirPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(System.out::println)
                    .forEach(File::delete);
        } catch (IOException e) {
            LOG.error("delete files in temporary directory failed.");
        }
    }

}











